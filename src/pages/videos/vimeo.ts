import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-vimeo',
    template: `
        <ion-header>
            <ion-navbar>
                <ion-title>Sci-fi & Tech</ion-title>
            </ion-navbar>
        </ion-header>
        <ion-content>
        <iframe src="https://player.vimeo.com/video/208791043?color=f7f7f7&title=0&byline=0&portrait=0" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </ion-content>
    `
})
export class VimeoVideo {
    constructor(public navCrtl: NavController) {}
}