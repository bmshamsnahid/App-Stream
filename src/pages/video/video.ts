import { VimeoVideo } from './../videos/vimeo';
import { YoutubeVideo } from './../videos/youtube';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'video.html'
})
export class VideoPage {

  public youtubeVideo: any = YoutubeVideo;
  public vimeoVideo: any = VimeoVideo;

  constructor(public navCtrl: NavController) {

  }
  
  playYoutube() {
    this.navCtrl.push(this.youtubeVideo);
  }

  playVimeo() {
    console.log('On click play vimeo');
    this.navCtrl.push(this.vimeoVideo);
  }

}
